---
layout: page
title: About
---

My name is Nate Palmer. I am a programmer that lives in Atlanta Georgia. I am originally from North East Pennslvyania. My career started when I got an internship at a local software company in rural PA. Originally I was helpdesk IT, but moved to programming quickly. 

I live and thrive in the dotnet ecosystm using C#. I have been developing full stack applications since the beginning. It started with asp.net MVC 3 and bringing me up to asp.net core 5. 

My wife has recently gone back to college for computer science. I will use this blog to write articles that would help someone like my wife. When she asks me a question, I will use that as a blog topic. I remember how hard it was navigating the software world when I first started. 